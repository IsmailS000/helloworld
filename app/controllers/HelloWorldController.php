<?php

class HelloWorldController extends \BaseController {
	public function index()
	{
		$Req_Name = Request::input('name');
		$Greeting = HelloWorldModel::Get_Greeting($Req_Name);
		return View::make('HelloWorldView')->with('Greeting', $Greeting);
	}
}

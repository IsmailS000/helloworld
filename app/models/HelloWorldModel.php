<?php

class HelloWorldModel {
	public static function Get_Greeting($Name)
	{
		if ($Name != "") {
			return 'Hello, '.$Name.'!';
		} else {
			return 'Hello, World!';
		}
	}
}

<!doctype html>
<html class="no-js" lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>HelloWorld</title>
    		<link rel="stylesheet" href="css/foundation.css" />
    		<script src="js/vendor/modernizr.js"></script>
	</head>
	<body>
		<div class="row">
			<div class="large-12 columns" style="background-color:#F1F1F1">
				<div class="panel">
					<h3>Intructions</h3>
					<p>Add the following to the URL: helloworld?name=<i>YourName</i></p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="large-12 columns" style="background-color:#F7F7F7;text-align:center;">
				<div class="panel">
					<img data-interchange="[img/small.png, (small)], [img/medium.png, (medium)], [img/large.png, (large)]">
				</div>
			</div>
		</div>

		<script src="js/vendor/jquery.js"></script>
		<script src="js/foundation.min.js"></script>
		<script>
			$(document).foundation();
		</script>
	</body>
</html>